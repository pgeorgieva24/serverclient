﻿using Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Server
{
    class TcpServer
    {
        private TcpListener _server;

        List<ClientWorking> listWorkingClients = new List<ClientWorking>();

        public TcpServer(int port)
        {
            _server = new TcpListener(IPAddress.Any, port);
            _server.Start();

            LoopClients();
        }

        void LoopClients()
        {
            while (true)
            {
                // wait for client connection
                TcpClient newClient = _server.AcceptTcpClient();
                ClientWorking clientWorking = new ClientWorking(newClient);

                listWorkingClients.Add(clientWorking);
                UpdateClientsChatPals();

                Thread t = new Thread(new ParameterizedThreadStart(HandleClient));
                t.Start(clientWorking);
            }
        }


        void HandleClient(object obj)
        {
            // retrieve client from parameter passed to thread
            ClientWorking client = (ClientWorking)obj;
            Console.WriteLine("another client just joined us ");
            client.BeginCommunication().Wait();

            
            listWorkingClients.Remove(client);
            UpdateClientsChatPals();
        }

        private void UpdateClientsChatPals()
        {
            listWorkingClients.ForEach(x => x.UpdateChatPalsAsync(listWorkingClients).Wait());
        }

        //private async Task ForwardMessageToOthers()
        //{
        //    var data = default(string);
        //    while (!((data = streamReader.ReadLine())).Equals("exit", StringComparison.OrdinalIgnoreCase))
        //    {
        //        if (chatPals.Count == 0)
        //            await ReceiveMessage("Chat room is empty :(");
        //        chatPals.ForEach(async x => await x.ReceiveMessage(this, data));

        //        Console.WriteLine(name + ": " + data);
        //    }
        //}
    }
}
