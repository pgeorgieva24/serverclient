﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            TcpClient client = new TcpClient();
            client.Connect("127.0.0.1", 23);

            
            //ClientWorking workingClient = new ClientWorking(client);
            Console.ReadLine();
        }
    }
}
