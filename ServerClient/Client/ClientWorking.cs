﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class ClientWorking
    {
        TcpClient _client;
        string name;

        StreamReader streamReader;
        StreamWriter streamWriter;
        List<ClientWorking> chatPals = new List<ClientWorking>();

        public ClientWorking(TcpClient client)
        {
            _client = client;
            streamReader = new StreamReader(_client.GetStream());
            streamWriter = new StreamWriter(_client.GetStream());

        }

        public async Task UpdateChatPalsAsync(List<ClientWorking> updatedChatPals)
        {
            chatPals.AddRange(updatedChatPals.Where(x => !chatPals.Contains(x) && x != this));

            var exited = chatPals.Where(x => !updatedChatPals.Contains(x)).FirstOrDefault();
            if (exited != null)
            {
                chatPals.Remove(exited);
                await ReceiveMessage(exited.name + " has left the group");
            }

        }

        public async Task BeginCommunication()
        {
            try
            {
                ReceiveMessage("Hi. You have successfuly reached our chat. To leave write 'exit'.").Wait();
                SetName().Wait();
                chatPals.ForEach(async x => await x.ReceiveMessage(name + " has joined the group"));
                var data = default(string);
                while (!((data = streamReader.ReadLine())).Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    if (chatPals.Count == 0)
                        await ReceiveMessage("Chat room is empty :(");
                    chatPals.ForEach(async x => await x.ReceiveMessage(this, data));

                    Console.WriteLine(name + ": " + data);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            finally
            {
                _client.Close();
            }
        }

        async Task SetName()
        {
            await ReceiveMessage("Enter your name:");
            name = await streamReader.ReadLineAsync().ConfigureAwait(false);
        }

        async Task ReceiveMessage(string message)
        {
            await streamWriter.WriteLineAsync(message);
            await streamWriter.FlushAsync().ConfigureAwait(false);
        }

        async Task ReceiveMessage(ClientWorking sender, string message)
        {
            try
            {
                await streamWriter.WriteLineAsync(GetMessageWithSenderName(sender.name, message));
                await streamWriter.FlushAsync().ConfigureAwait(false);

            }
            //dont know what ind of exception to catch
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        string GetMessageWithSenderName(string name, string message)
        {
            return name + ": " + message;
        }
    }
}
